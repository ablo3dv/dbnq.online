<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Laravel\Socialite\Facades\Socialite;

class DashboardController extends Controller
{

    public function index(Request $request)
    {
        return view('dashboard');
    }
}
