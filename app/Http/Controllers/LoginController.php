<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Laravel\Socialite\Facades\Socialite;


class LoginController extends Controller
{

    public function index(Request $request)
    {
        // on verifie si il y un utilisateur est connecte
        if (Auth::check()) {
            // il y a un utilisateur connecte
            // donc on le redirige vers le dashboard
            return redirect()->route('dashboard');
        }else{
            // il n'y a pas d'utilisateur connecte
            // on affiche donc la page de connection
            return view('login');
        }
    }

    public function facebook_login(Request $request){
        return Socialite::driver('facebook')->redirect();
    }

    public function facebook_login_return(Request $request){
        $user = Socialite::driver('facebook')->user();

        $connected_user = User::where('social_id','=',''.$user->getId())->get()->first();
        if($connected_user === null){
            $connected_user = new User();
            $connected_user->social_id =  $user->getId();
            $connected_user->email = $user->getEmail();
            $connected_user->name = $user->getName();
            $connected_user->password = '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi'; // password
            $connected_user->save();
        }else{

        }

        if (Auth::attempt(['email' => $user->getEmail(), 'social_id' => $user->getId(), 'password' => 'password'])) {
            // The user is active, not suspended, and exists.
            $user = Auth::user();
            //dd($user);
            return redirect()->route('dashboard');
        }else{
            return redirect()->route('login');
        }



    }

    public function logout(Request $request){
        Auth::logout();
        return redirect()->route('login');
    }
}
