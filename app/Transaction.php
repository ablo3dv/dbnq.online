<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Transaction extends Model
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'amount', 'balance_before', 'balance_after', 'account_id', 'related_transaction_id', 'operation_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [

    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'amount' => 'double',
        'balance_before' => 'double',
        'balance_after' => 'double',
    ];

    public function account()
    {
        return $this->belongsTo(\App\Account::class);
    }

    public function operation()
    {
        return $this->belongsTo(\App\Operation::class);
    }


}
